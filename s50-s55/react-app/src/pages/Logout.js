import { Navigate } from 'react-router-dom';

export default function logout(){
	// Remove the email from the local storage.
	localStorage.clear();

	return (
		// After removing the email from the local storage, it will redirect us to /login
		<Navigate to="/login" />
	)
}