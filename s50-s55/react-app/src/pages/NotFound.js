export default function notfound(){
	return (
		<>
		<h2>Page Not Found</h2>
		<p>Go back to the <a href="/">homepage</a></p>
		</>
		)
}