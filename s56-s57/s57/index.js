// Question: What would be the output of the loop?
for (let i = 1; i < 5; i++) {
  console.log(i * 1);
}
// Answer1: The loop will print the numbers from 1 to 4, each multiplied by 1.
// Output:
// 1
// 2
// 3
// 4


// Question: What would be the problem in the code snippet?
let students = ['John', 'Paul', 'George', 'Ringo'];
console.log('Here are the graduating students:');
for (let count = 0; count <= students.length; i++) {
  console.log(students[count]);
}
// Answer2: There is a typo in the loop. Instead of 'i++', it should be 'count++' to increment the loop counter.
// The loop condition should also use '<' instead of '<=' to avoid going out of bounds.
// Additionally, 'i' should be declared before using it in the loop.
// Corrected code:
// for (let count = 0; count < students.length; count++) {
//   console.log(students[count]);
// }


// Question: What would be the console output of the function?
function checkGift(day) {
  let gifts = ['partridge in a pear tree', 'turtle doves', 'french hens', 'golden rings'];
  if (day > 0 && day < 4) {
    return `I was given ${day} ${gifts[day - 1]}`;
  } else {
    return `No gifts were given`;
  }
}
checkGift(3);
// Answer3: The function 'checkGift' checks if the given 'day' is between 1 and 3 (inclusive) and returns the corresponding gift. 
// In this case, 'checkGift(3)' will return "I was given 3 french hens" since 'gifts[2]' is "french hens".


// What would be the problem in the code snippet?
let items = [/* ... */];
for (let i = 0; i < items.length; i++) {
  console.log(`
    Name: ${items[i].name}
    Description: ${items[i].description}
    Price: ${items[i].price}
  `);
}
// Answer4: There is a duplicate 'id' of 1 in the 'items' array, which is incorrect. Each 'id' should be unique.


// Question: What would be the output?
for (let row = 1; row < 3; row++) {
  for (let col = 1; col <= row; col++) {
    console.log(`Current row: ${row}, Current col: ${col}`);
  }
}
// Answer5: This nested loop will print the row and column numbers for each iteration.
// Output:
// Current row: 1, Current col: 1
// Current row: 2, Current col: 1
// Current row: 2, Current col: 2


// Question: What would be the problem in the code snippet?
function checkLeapYear(year) {
  if (year % 4 = 0) {
    if (year % 100 = 0) {
      if (year % 400 = 0) {
        console.log('Leap year');
      } else {
        console.log('Not a leap year');
      }
    } else {
      console.log('Leap year');
    }
  } else {
    console.log('Not a leap year');
  }
}
checkLeapYear(1999);
// Answer6: There are syntax errors in the code. The comparison operators should be '===' instead of '=' within the conditional statements.


// Question: Given the array below, how can the last student's English grade be displayed?
let records = [/* ... */];
// Answer7: To display the last student's English grade, you can access it using array indexing like this:
// console.log(records[records.length - 1].subjects[0].grade);


// Question: What would be the problem in the code snippet?
function checkDivisibility(dividend, divisor) {
  if (dividend % divisor == 0) {
    console.log(`${dividend} is divisible by ${divisor}`);
  } else {
    console.log(`${dividend} is not divisible by ${divisor}`);
  }
}
checkDivisibility(100, 0);
// Answer8: You should avoid division by zero, as it is mathematically undefined. The code should include a check to handle the case when 'divisor' is 0.