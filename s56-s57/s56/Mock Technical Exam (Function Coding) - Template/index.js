function countLetter(letter, sentence) {
    if (typeof letter !== 'string' || letter.length !== 1) {
        return undefined;
    }

    let count = 0;
    for (const char of sentence) {
        if (char.toLowerCase() === letter.toLowerCase()) {
            count++;
        }
    }
    return count;
}

function isIsogram(text) {
    const cleanedText = text.toLowerCase().replace(/\s/g, '');
    const charSet = new Set(cleanedText);

    return cleanedText.length === charSet.size;
}

function purchase(age, price) {
    if (age < 13) {
        return undefined;
    } else if (age >= 13 && age <= 21 || age >= 65) {
        return (price * 0.8).toFixed(2);
    } else {
        return price.toFixed(2);
    }
}

function findHotCategories(items) {
    const categories = {};
    const hotCategories = [];

    for (const item of items) {
        if (item.stocks === 0 && !categories[item.category]) {
            categories[item.category] = true;
            hotCategories.push(item.category);
        }
    }

    return hotCategories;
}

function findFlyingVoters(candidateA, candidateB) {
    const candidateAVotes = new Set(candidateA);
    const commonVotes = [];

    for (const vote of candidateB) {
        if (candidateAVotes.has(vote)) {
            commonVotes.push(vote);
        }
    }

    return commonVotes.sort();
}

module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};